package com.megaport.sortNamesApp;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.code.externalsorting.ExternalSort;

/**
 * A class that contains sorting implementations for a list of string.
 * The input to the methods is assumed to be
 * a file with a list of names in [LASTNAME,Firstname] format; each on a new line.
 * 
 * @author Ksenia Belikova
 * @version Februray 22, 2022
 */
public class App {
    
    /**
     * Internal sorting using Java 8 streams.
     * 
     * @param inputFile   file with input data
     * @param outputFile  file to output results into
     */
    private void sortInternally(
        String inputFile,
        String outputFile
    ) {

        try {
            File file = new File(inputFile);

            // Read file into stream - not ref in memory
            Stream<String> stream = Files.lines(file.toPath(), StandardCharsets.UTF_8);
            List<String> sortedList = stream.sorted().collect(Collectors.toList());

            FileWriter writer = new FileWriter(outputFile); 
            for(String str: sortedList) {
              writer.write(str + System.lineSeparator());
            }
            writer.close();

        } catch(Exception e1) {
            e1.printStackTrace();
        } 

    }


    /**
     * External sorting using a third party library by Google: 
     * https://github.com/lemire/externalsortinginjava
     * 
     * @param inputFile   file with input data
     * @param outputFile  file to output results into
     */
    private void sortExternally(
        String inputFile,
        String outputFile
    ) {

        try {
            File output = new File(outputFile);
            output.createNewFile(); // if file already exists will do nothing 
            FileOutputStream outputStream = new FileOutputStream(output, false); 

            // next command sorts the lines from inputfile to outputfile
            ExternalSort.mergeSortedFiles(
                ExternalSort.sortInBatch(new File(inputFile)), 
                output
            );

            outputStream.close();

        } catch(Exception e1) {
            e1.printStackTrace();
        } 
    }

    private Stream<String> readFileUsingStreams(File file) {

        Stream<String> stream = null;
        // Read file into stream - not ref in memory
        try {
            stream = Files.lines(file.toPath(), StandardCharsets.UTF_8);
            // total += stream.mapToInt(s -> LINE_DELIMITER_PATTERN.split(s, 0).length).sum();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        
        return stream;
    }
    
    public static void main(String args[]) {
        // Default values
        String inputFile = "input";
        String outputFile = "ouput";
        String sortType = "internal";

        // Parcing command line arguments
        if (args.length > 0) {
            inputFile = String.valueOf(args[0]);

            if (args.length > 1) { 
                outputFile = String.valueOf(args[1]);

                if (args.length > 2) { 
                    sortType = "external";
                }
            }   
        }

        int count = 0;
        // JVM wamrup
        for (int i = 0; i < 100000; i++) count++;

        App app = new App();

        switch(sortType)  {
            case "external": 
                app.sortExternally(inputFile, outputFile);
                break;
            case "internal": 
            default:
                app.sortInternally(inputFile, outputFile);
        }

    }
}